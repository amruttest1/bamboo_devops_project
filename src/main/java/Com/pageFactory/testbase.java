package Com.pageFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Listeners;

import Com.utility.ListenerClass;

public class testbase {
	
	public static final Logger log = Logger.getLogger(testbase.class.getName());
	static Properties prop;
	public static WebDriver driver= null;
	public static WebDriverWait wait;
	
	public void inti() throws FileNotFoundException, IOException
	{	
		loadData();		
		firefoxbrowser(getdata("browserName"));
		//System.out.println(getdata("URL"));
	}
		
	public static void loadData() throws IOException,FileNotFoundException
	{
		prop= new Properties();
		File f = new File(System.getProperty("user.dir")+"\\src\\main\\java\\Com\\config\\config.properties");
		FileReader obj= new FileReader(f);
		prop.load(obj);
		
		File f2 = new File(System.getProperty("user.dir")+"\\src\\main\\java\\Com\\config\\databasefile.properties");
		FileReader obj2= new FileReader(f2);
		prop.load(obj2);
	}
	public static String getdata(String Data) throws FileNotFoundException, IOException
	{
		loadData();
		String data= prop.getProperty(Data);
		return data;
			
	}
	public static WebDriver getDriver() {
		return driver;
	}
	
	public static WebDriver firefoxbrowser(String Browser) throws FileNotFoundException, IOException
	{	// open  firefox webdriver
		String driverpath =(System.getProperty("user.dir")+"\\geckodriver.exe");
		if(driver == null)
			{
				System.out.println(driverpath);
				System.setProperty("webdriver.gecko.driver",driverpath);			
				FirefoxOptions options = new FirefoxOptions();
				options.setHeadless(true);
				driver= new FirefoxDriver(options);
				options.addArguments("window-size=1200x600");
				driver.get(getdata("URL"));
		}
		return driver;
		 
	}

	@AfterSuite
	public void closeBrowser()
	{
		driver.close();
	}
		
}
