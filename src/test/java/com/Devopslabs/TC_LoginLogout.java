package com.Devopslabs;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Com.pageFactory.testbase;
public class TC_LoginLogout extends testbase{
	//public static WebDriver driver= null;
	SoftAssert assertion= new SoftAssert();
	
	
//	@FindBy(xpath="/html/body/div[1]/table/tbody/tr/td[1]")
//    WebElement ele_Todo;
	
	 @BeforeSuite
		public void setUP() throws IOException{
			inti();
	}
	
//	Defining driver and url
	//@BeforeClass
	 public static WebDriver chromeBrowser() 
	 { 	
			if(driver == null){
			
				System.setProperty("webdriver.gecko.driver", "geckodriver.exe");			
				driver= new FirefoxDriver();					
				String baseUrl= "http://devopslabs.co:8888/welcome";
				driver.get(baseUrl);
			}
			return driver;
	 	}
	//login to jira 
	//	@Test(priority = 1)
		 public void TC_001_VarifyToDo()
		 {	
			driver=chromeBrowser() ;
		 	 try
		 	 {  
		 		 WebElement ele_Todo=driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td[1]"));
		 		 System.out.println(ele_Todo.getText());
				assertion.assertTrue(ele_Todo.isDisplayed(),ele_Todo.getText());
				assertion.assertAll();
				//logout();
	 	 }
	 	catch(Exception e)
	 	{
	 		e.printStackTrace();
	 		
	 	}
	}
		 @Test()
		 public void EditButton()
		 {
	
	     WebElement btnedit=driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td[4]/a"));
//	     btnsearch.click();
	     	assertion.assertTrue(btnedit.isDisplayed(),btnedit.getText());
	       assertion.assertAll();
		 }
		 
		 
	//@AfterClass
	public void BrowserClose()
	{
		driver.close();
	}
	
}
